> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Application Development

## Spencer Aviles 

### Assignment 1  Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Install Android Studio
3. Install Java

#### README.md file should include the following items:

* Screenshot of JDK Java Hello
* Screenshot running Android Studio My First App
* Screenshots of running Android Studio Contacts App
* git commands w/ short descriptions
* Bitbucket repo links
  a) This assignment
  b) The completed tutorial repo (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates an empty Git repository or reinitializes an existing one
2. git status - Shows the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git merge - Join two or more development histories together

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](java_hello.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](android_MyFirstApp.png)

*Screenshot of Android Studio - Contacts App:
![Android Studio Contacts App Screenshot](home.png)
![Spencer Screenshot](spencer.png)
![Pat Screenshot](pat.png)
![Justin Screenshot](justin.png)
![Jack Screenshot](jack.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/SpencerAviles/bitbucketstationlocations/src/master)