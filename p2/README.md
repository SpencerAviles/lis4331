> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Applications Development

## Spencer Aviles 

### Project 2 Requirements:

1. Create a splash screen for your App
2. Create an app that allows you to add, update, and delete users from a database

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of adding of username and password
* Screenshot of updating username
* Screenshot of viewing the data 
* Screenshot of deleting user

#### Assignment Screenshots:

*Screenshot Splash Screen and Opening Screen*

![Splash Screen](splash.png)  	      ![Open](open.png)     

*Add User "spa17b"*

![Add](add.png)			![First View](first.png)

*Update User*

![Update](update.png)		![View](view.png)

*Deleting the User*

![Delete](delete.png)		![Final](final.png)