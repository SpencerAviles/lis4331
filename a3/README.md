> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Applications Development

## Spencer Aviles 

### Assignment 3 Requirements:

1. Create an app that converts US Dollars into Euros, Pesos, or Canadian Dollars
2. Create a splash screen for Currency Converter app
3. Skillsets 4-6

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's unpopulate user interface
* Screenshot of running application's toast notification
* Screenshot of running application's converted currency interface
* Screenshots of Skill Sets 4-6

#### Assignment Screenshots:

*Screenshot of Currency Conversion App:*

![Splash Screen](splash.png) ![Unpopulated Home Screen](unpop.png)

![Toast Screen](toast.png) ![Converted Screen](conversion.png)

*Skill Set 4*

![Part 1](ss4_1.png) ![Part 2](ss4_2.png)

*Skill Set 5*

![Part 1](ss5_1.png) ![Part 2](ss5_2.png)
![Part 3](ss5_3.png) ![Part 4](ss5_4.png)

*Skill Set 6*

![Part 1](ss6_6.png) ![Part 2](ss6_1.png)
![Part 3](ss6_2.png) ![Part 4](ss6_3.png)
![Part 5](ss6_4.png) ![Part 6](ss6_5.png)