> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4331 Advanced Mobile Application Development

## Spencer Aviles 

### LIS 4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Tip Calculator App
    - Skillset 1 - Java: Non-OOp Circle
    - Skillset 2 - Java: Multiple Number
    - Skillset 3 - Java: Nested Structures

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Currency Conversion App
    - Skillset 4 - Java: Time Conversion
    - Skillset 5 - Java: Even or Odd (GUI)
    - Skillset 6 - Java: Paint Calculator (GUI)

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create a Mortgage Interest Calculator
    - Skillset 10 - Java: Travel Time
    - Skillset 11 - Java: Product Class
    - Skillset 12 - Java: Book Inherits Product Class

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create an app with a title and a list of articles
    - Use RSS feed
    - Skillset 13 - Java: Write/Read File
    - Skillset 14 - Java: Simple Interest Calculator
    - Skillset 15 - Java: Array Demo Using Methods  

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create Music App
    - Skillset 7 - Java: Measurement Conversion
    - Skillset 8 - Java: Distance Calculator (GUI)
    - Skillset 9 - Java: Multiple Selection Lists (GUI)

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create an app that adds, updates, and deletes user entered data into a database
    - Provide a splash screen 

