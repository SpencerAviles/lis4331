> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Applications Development

## Spencer Aviles 

### Project 1 Requirements:

1. Create a splash screen for your Music app
2. Create an app that plays mp3 files for 4 different songs
3. Should be able to pause and play songs

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's opening screen
* Screenshot of running application's playing screen
* Screenshot of running application's pause screen
* Screenshots of Skill Sets 7-9

#### Assignment Screenshots:

*Screenshots of Music App Splash Screen and Opening Screen:*

![Splash Screen](splash.png)	 ![Opening](opening.png)


*Screenshots of Playing and Paused Screens*

![Playing](playing.png)	    ![Paused](paused.png)


*Skill Sets*


*SkillSet 7*

![SS7](ss7.png)


*SkillSet 8*

![Part 1](ss8_p1.png)		![Part 2](ss8_p2.png)


*SkillSet 9*

![Part 1](ss9_p1.png)

![Part 2](ss9_p2.png)

![Part 3](ss9_p3.png)