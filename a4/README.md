> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Applications Development

## Spencer Aviles 

### Assignment 4 Requirements:

1. Create Splash Screen
2. Create Mortgage Interest Calculator
3. Have the app calculate your interest paid
4. Skill Sets 10-12

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's home screen
* Screenshot of running application's invalid screen
* Screenshot of running application's valid screen
* Screenshots of Skill Sets 10-12

#### Assignment Screenshots:

*Screenshot of App:*

![Splash Screen](splash.png) ![Unpopulated Home Screen](home.png)

![Invalid Screen](invalid.png) ![Valid Screen](valid.png)

*Skill Set 10*

![Part 1](ss10.png)

*Skill Set 11*

![Part 1](ss11.png) 

*Skill Set 12*

![Part 1](ss12.png) 