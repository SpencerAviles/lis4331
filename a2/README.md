> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 Advanced Mobile Applications Development

## Spencer Aviles 

### Assignment 2 Requirements:

1. Create an app that calculates the cost per person for a restaurant bill plus tips
2. App should include at least 10 guests and 25% tips(5% increments)
3. Backward engineer Skillsets 1 through 3

#### README.md file should include the following items:

* Screenshot of application's unpopulated user interface
* Screenshot of application's populated user interface
* Screenshots of Skillsets 1, 2, and 3


#### Assignment Screenshots:

*Screenshot of Tip Calculator:*

![Unpopulated UI Screenshot](a2_home.png)![Populated UI Screenshot](app_w_values.png)

*Screenshot of Skillset 1:*

![SS1 Screenshot](ss1.png)




*Screenshot of Skillset 2:*

![SS2 Screenshot](ss2.png)




*Screenshot of Skillset 3:*

![SS3 Screenshot](ss3.png)